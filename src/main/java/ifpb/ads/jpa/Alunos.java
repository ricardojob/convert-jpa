package ifpb.ads.jpa;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

/**
 *
 * @author Ricardo Job
 */
@Entity
@NamedQuery(name = "aluno.todos",query = "Select a From Alunos a")
public class Alunos implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nome;
    

     
    @Column(name = "data_aniversario")
    @Convert(converter = LocalDateConverter.class)
    private LocalDate aniversario;

    public Alunos() {
    }

    public Alunos(String nome, String matricula) {
        this(nome, LocalDate.now());
    }

    public Alunos(String nome, LocalDate aniversario) {
        this.nome = nome; 
        this.aniversario = aniversario; 
    }
 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getAniversario() {
        return aniversario;
    }

    public void setAniversario(LocalDate aniversario) {
        this.aniversario = aniversario;
    }

     
    @Override
    public String toString() {
        return "Aluno{" + "id=" + id + ", nome=" + nome+ '}';

    }

}
