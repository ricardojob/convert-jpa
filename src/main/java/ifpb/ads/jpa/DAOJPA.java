package ifpb.ads.jpa;

import com.sun.javafx.scene.EnteredExitedHandler;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * @author job
 */
public class DAOJPA {

    private EntityManager entityManager;

    public DAOJPA() {
        this("desenvolvimento");
    }

    public DAOJPA(String unidadePersistencia) {
        entityManager = Persistence.createEntityManagerFactory(unidadePersistencia).createEntityManager();

    }

    public boolean salvar(Alunos obj) {
        EntityTransaction transacao = entityManager.getTransaction();

        try {
            transacao.begin();
            entityManager.persist(obj);
            transacao.commit();
            return true;
        } catch (Exception ex) {
            if (transacao.isActive()) {
                transacao.rollback();
            }
            return false;
        }

    }

    

    public List<Alunos> buscar() {
        return entityManager.createNamedQuery("aluno.todos", Alunos.class).getResultList();
    }

}
